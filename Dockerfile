FROM golang:1.16.3-alpine

# Create and change to the app directory.
WORKDIR /app

# Retrieve application dependencies.
# This allows the container build to reuse cached dependencies.
# Expecting to copy go.mod and if present go.sum.
COPY go.* ./
RUN go mod download

# Copy local code to the container image.
COPY . ./

# Build the binary.
RUN go build -v -o main

# Export necessary port
EXPOSE 80

# Command to run when starting the container
CMD ["/app/main"]