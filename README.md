# Go Web Server

Boilerplate for web server written in Go.

## Local Development

1. Install the latest version of [Docker](https://www.docker.com/products/docker-desktop).
2. Edit `/etc/hosts` and add the following at the bottom:
   
`127.0.0.1 app.local`

3. From the repository call:
   
`docker-compose -f local-compose.yaml up --build -d`.

You should now be able to visit a locally hosted app at [http://app.local:8080](http://app.local:8080).

To stop and remove the running containers, execute:
1. `docker stop $(docker ps -a -q)`
2. `docker rm $(docker ps -a -q)`

## Deployment

1. Clone this repository to your server.
2. From the repository folder, execute the following:

`docker-compose up -d`

You should now be able to visit the app at [https://app.EXAMPLE.xyz](https://app.EXAMPLE.xyz).
